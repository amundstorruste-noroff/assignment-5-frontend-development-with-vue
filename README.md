# Assignment 5

Author: Amund Kulsrud Storruste

## Background

The project contains the delivery for Assignment 5 in the Experis Academy accellerate course at Noroff University.

Assignment 5 is a part of Module 6 -  Frontend Development with Vue

## Install

This project uses [node](http://nodejs.org) and [npm](https://npmjs.com). Go check them out if you don't have them locally installed.

To run the project in development mode first install dependencies:

```sh
$ npm install
```

Then run the project on port 3000:
```sh
$ npm run dev
```

## Usage

The project uses Vue and Typescript and has been deployed as a Heroku app.

The app can be found on the following url: [Travilol Trivia Game](https://mysterious-fjord-97290.herokuapp.com/ "The Trivia Game for people that like Times New Roman")

Enter a username in the `Enter username here...` fild to start a trivia Quiz.

Answer each question to the best of your ability!

Click the submit button when each answer has been chosen.

Check the high score list to see how you did.

Play again by clicking the `Play Again button`
