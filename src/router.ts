import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import QuestionScreen from "./pages/QuestionScreen.vue";
import ResultScreen from "./pages/ResultScreen.vue";
import StartScreen from "./pages/StartScreen.vue";

const routes: RouteRecordRaw[] = [
	{
		path: "/",
		component: StartScreen,
	},
    {
		path: "/Questions",
		component: QuestionScreen,
	},
    {
		path: "/Results",
		component: ResultScreen,
	},
];

export default createRouter({
	history: createWebHistory(),
	routes,
});
