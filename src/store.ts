import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import { TriviaQuestion } from "./types";

// Define typing for the store state
export interface State {
    username: string;
    questions: TriviaQuestion[];
    userAnswers: string[];
}

// define injection key necessary for TypeScript config
export const key: InjectionKey<Store<State>> = Symbol();

/**
 * Initialize a store for handling global variables. 
 * The username, and a list of questions and userAnswers are stored.
 * They are therefore accessible in all subcomponents fo App.vue.
 */
export const store = createStore<State>({
    state: {
        username: "",
        questions: [],
        userAnswers: []
    },
    mutations : {
        setUsername: (state: State, payload: string) => {
            state.username = payload;
        },
        setQuestions: (state: State, payload: TriviaQuestion[]) => {
            state.questions = [...payload];
        },
        setUserAnswers: (state: State, payload: string[]) => {
            state.userAnswers = [...payload];
        }
    }
})

// define your own `useStore` composition function
export function useStore () {
    return baseUseStore(key)
  }