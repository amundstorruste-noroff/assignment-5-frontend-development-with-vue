export interface UserScore {
    id: number;
    username: string;
    highScore: number;
}

export interface UserScoreWithoutId {
    username: string;
    highScore: number;
}

export interface TriviaQuestion {
    id: string;
    type: "bolean" | "multiple";
    question : string;
    correct_answer: string;
    incorrect_answers: string[];
    answer_options: string[];
}