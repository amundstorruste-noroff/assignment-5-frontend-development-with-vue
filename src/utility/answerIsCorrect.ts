import { TriviaQuestion } from "../types";

/**
 * Checks if an answer is the same as the correct_answer indicated in a TriviaQuestion.
 * Returns true on a match, and false otherwise.
 */
 const answerIsCorrect = (answer: string, question: TriviaQuestion) : boolean => {
    const correctAnswer = question.correct_answer;
    return answer.trim() === correctAnswer.trim()
}

export default answerIsCorrect;