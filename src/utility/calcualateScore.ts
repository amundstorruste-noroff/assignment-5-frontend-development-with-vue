import { TriviaQuestion } from "../types";

/**
 * Calculates the score of an answered Trivia run. 10 points per correct answer.
 * @param questions
 * @param userAnswers
 */
const calculateScore = (
	questions: TriviaQuestion[],
	userAnswers: string[]
): number => {
	let score = 0;
	// Zip correct answers and user answers together
	const zippedLists = questions.map((question, idx) => [
		question.correct_answer,
		userAnswers[idx],
	]);
	// Loop through and see how many right we have
	for (const [correct_answer, userAnswer] of zippedLists) {
		if (correct_answer === userAnswer) score += 10;
	}
	return score;
};

export default calculateScore;
