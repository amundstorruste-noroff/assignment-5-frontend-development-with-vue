import axios from "axios";
import { TriviaQuestion } from "../types";
import shuffleArray from "./shuffleArray";

export interface TriviaQuestionResponse {
    response_code : number;
    results : TriviaQuestion[];
}

/**
 * Fetches 10 trivia questions from the optentdb API
 * @returns 
 */
export const fetchTriviaQuestionsAsync = async ():Promise<[string | null, TriviaQuestion[]]> => {
    const TRIVIA_URL = "https://opentdb.com/api.php?amount=10";
    try {
        const { data } = await axios.get<TriviaQuestionResponse>(TRIVIA_URL);
        const questions = data.results.map((question) => {
                return {
                    ...question,
                    id: Math.random().toString(16).slice(2), // Dewalds Magically generated ID
                    answer_options: shuffleArray([question.correct_answer, ...question.incorrect_answers])                    
                }
            }
        )
        return [null, questions];
    } catch ( error: any ) {
        return [error.message, []];
    }
}
