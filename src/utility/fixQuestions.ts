import { TriviaQuestion } from "../types";

/**
 * Fixes unicode characters in questions in place
 * @param questions 
 * @returns 
 */
 const fixQuestions = (questions: TriviaQuestion[]) : TriviaQuestion[] => {
    const fixedQuestions: TriviaQuestion[] = []
    for (let triviaQuestion of questions){
        let text = triviaQuestion.question;
        (text.match(/&.+;/ig) || []).forEach(entity => { 
            let temp = document.createElement("div")
            // Insert the HTML entity as HTML in an HTML element:
            temp.innerHTML = entity;
            
            // Retrieve the HTML elements innerText to get the parsed entity (the actual character):
            text = text.replace(entity, temp.innerText);
        });
        triviaQuestion.question = text;
    }
    return fixedQuestions
 }
 
 export default fixQuestions;