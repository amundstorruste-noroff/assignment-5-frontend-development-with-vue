/**
 * Checks if an answer is the same as the userAnswer selected by a user.
 * @param answer 
 * @param userAnswer 
 */
const isUserAnswer = (answer: string, userAnswer: string) : boolean => {
   return answer == userAnswer 
}

export default isUserAnswer;