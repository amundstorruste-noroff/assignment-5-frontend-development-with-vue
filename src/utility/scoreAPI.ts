import axios from "axios";
import { UserScore, UserScoreWithoutId } from "../types";

// URL to where the scores are stored
const apiURL = "https://aks-noroff-assignment-api.herokuapp.com/trivia";
// Header configuration for creating, updating, and deleting entries.
const config = {
	headers: {
		"X-API-Key": "CutiJyXdiUmy2nHRgONcJg==",
	},
};

/**
 * Fetch all scores currently recorded on the high-score API.
 * @returns 
 */
export const getAllScoresAsync = async (): Promise<[string | null, UserScore[]]> => {
    try {
        const { data } = await axios.get<UserScore[]>(apiURL);
        return [null, data]
    } catch ( error: any ) {
        return [error.message, []];
    }
};

/**
 * Fetch the score of a given user
 * @param username 
 * @returns 
 */
export const getScoreAsync = async (username: string): Promise<[string | null, UserScore[]]> => {
    try {
        const { data } = await axios.get<UserScore[]>(apiURL + "/?username=" + username);
        return [null, data]
    } catch ( error: any ) {
        return [error.message, []];
    }
};

/**
 * Post score for a new user.
 * @param payload 
 * @returns 
 */
export const postScoreAsync = async (payload: UserScoreWithoutId): Promise<[string | null, UserScore[]]> => {
    try {
        const { data } = await axios.post<UserScore[]>(apiURL, payload, config);
        return [null, data]
    } catch ( error: any ) {
        return [error.message, []];
    }
};

/**
 * Update the score of an already existing user
 * @param payload 
 * @returns 
 */
export const putScoreAsync = async (payload: UserScore): Promise<[string | null, UserScore[]]> => {
    try {
        const { data } = await axios.put<UserScore[]>(apiURL + "/" + payload.id, payload, config);
        return [null, data]
    } catch ( error: any ) {
        return [error.message, []];
    }
};

/**
 * Delete the score of an existing user
 * @param id 
 * @returns 
 */
export const deleteScoreAsync = async (id: number): Promise<[string | null, UserScore[]]> => {
    try {
        const { data } = await axios.delete<UserScore[]>(apiURL + "/" + id, config);
        return [null, data]
    } catch ( error: any ) {
        return [error.message, []];
    }
};
