/**
 * Loops through a list and has a 50% chance of switching two elements at every step
 * @param arr
 * @returns
 */
const shuffleArray = (arr: any[]) => {
	return arr.sort(() => 0.5 - Math.random());
};

export default shuffleArray;
