
import { getScoreAsync, postScoreAsync, putScoreAsync } from "./scoreAPI";
import { UserScore, UserScoreWithoutId } from "../types";

export const submitScoreToAPI = async (newScore: number, username: string) => {
	// Check if user is already registered
	const [_, data] = await getScoreAsync(username);

	// If no prior record
	if (data.length === 0) {
		const userScore: UserScoreWithoutId = {
			username: username,
			highScore: newScore,
		};
		return postScoreAsync(userScore);
	}

	// Break if old score is better
	const userScore: UserScore = { ...data[0] };
	if (userScore.highScore > newScore) return;

	// update API with new highscore
	userScore.highScore = newScore;
	return putScoreAsync(userScore);
};
