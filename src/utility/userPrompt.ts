/**
 * Returns a user prompt based on a score from 0-100
 * @param score 
 * @returns 
 */
const userPrompt = (score: number): string => {
    if (score === 100) return "Wow! Impressive! You are a true god of Travilol! Dewald Els will be contacting you regarding a job offer as Lecturer at Noroff University!"
    if (score >= 80) return "Wow! Very good, you should be proud."
    if (score >= 50) return "Nice! You know quite a bit of unecessary stuff."
    if (score >= 30) return "Poor showing. It's ok, we can't all be great. Try reading a book."
    if (score >= 10) return "Impressive! You nearly failed all the questions! Luckily some of your guesses must have gone in your favor! Go educate yourself..."
    return "Lol. Pleb. True shit tier performance. You're probably the kind of person who uses vanilla Javascript without Typescript. Please uninstall."
}

export default userPrompt;
